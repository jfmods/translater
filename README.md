Translater takes all the text in minecraft and runs it through Google Translate 5 times (configurable).
This leaves the text in a state that is still partially understandable but hilarious to play with.
The mod caches all responses from Google Translate to reduce loading times as creating the cache can take up to 3 hours for vanilla (~5000 pieces of text)
This allows it to have no major impact on loading/performance after generating the cache.
In fact, a pre-generated name cache is even contained in the mod and will be used if possible.
You can also download the additional file with pre-cached responses for most mods to improve speeds at the cost of a bit more disk space.
Please note that generating the additional file takes some time, so it might not be available right after an update
This should work fine with most other mods that use the vanilla system for translation.

You can configure the following things:
- The amount of times all text should be translated (defaults to 10, requires cache recreation)
- Whether to use random languages as source languages (this causes most text to be unrecognizable, requires cache recreation)
- The target language for translations, should also equal the games language. You can use "auto" to attempt automatic detection
- Progress indicators for translations to watch over the initial generation