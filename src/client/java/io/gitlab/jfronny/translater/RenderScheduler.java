package io.gitlab.jfronny.translater;

import io.gitlab.jfronny.translater.mixin.MinecraftClientAccessor;
import net.minecraft.client.MinecraftClient;

import java.util.concurrent.atomic.AtomicBoolean;

public class RenderScheduler {
    private final AtomicBoolean shouldRun = new AtomicBoolean(false);

    public void scheduleRender() {
        shouldRun.set(true);
        MinecraftClient mc = MinecraftClient.getInstance();
        if (mc != null && mc.isOnThread()) performTask();
    }

    public void deschedule() {
        shouldRun.set(false);
    }

    private void performTask() {
        do {
            ((MinecraftClientAccessor) MinecraftClient.getInstance()).invokeRender(false);
        } while (shouldRun.compareAndSet(true, false));
    }
}
