package io.gitlab.jfronny.translater;

import io.gitlab.jfronny.libjf.translate.api.TranslateService;
import io.gitlab.jfronny.translater.transformer.CachingTransformer;
import io.gitlab.jfronny.translater.transformer.FailTransformer;
import io.gitlab.jfronny.translater.transformer.TransformingMap;
import io.gitlab.jfronny.translater.transformer.TranslatingTransformer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Environment(EnvType.CLIENT)
public class Translater {
    public static final String MOD_ID = "translater";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);
    private static final TransformingMap map;

    static {
        TranslateService<?> ts = TranslateService.getConfigured();
        if (ts == null || ts.getName().equals("Noop")) {
            LOGGER.error("No translation service found! Please configure a translation service in the libjf-translate config and restart the game!\nThis means NO NEW TRANSLATIONS WILL BE GENERATED!");
            map = new TransformingMap(new CachingTransformer(new FailTransformer()));
        } else {
            map = new TransformingMap(new CachingTransformer(new TranslatingTransformer<>(ts)));
        }
    }

    public static @NotNull TransformingMap getMap(@Nullable Map<String, String> base) {
        if (base != null) map.updateBacker(base);
        return map;
    }
}
