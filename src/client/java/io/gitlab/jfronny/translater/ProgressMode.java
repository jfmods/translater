package io.gitlab.jfronny.translater;

public enum ProgressMode {
    Full,
    Gui,
    Console,
    None
}
