package io.gitlab.jfronny.translater.mixin;

import net.minecraft.client.gl.ShaderLoader;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(ShaderLoader.Cache.class)
public interface ShaderLoaderCacheAccessor {
    @Accessor("definitions") ShaderLoader.Definitions getDefinitions();
}
