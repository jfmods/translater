package io.gitlab.jfronny.translater.mixin;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.translater.Cfg;
import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Plugin implements IMixinConfigPlugin {
    @Override
    public void onLoad(String mixinPackage) {
    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        LibJf.setup();
        final String prefix = "io.gitlab.jfronny.translater.mixin.";
        if (!mixinClassName.startsWith(prefix)) throw new IllegalStateException("Invalid mixin package for " + mixinClassName);
        return switch (mixinClassName.substring(prefix.length())) {
            case "LanguageMixin", "TranslationStorageAccessor" -> true;
            case "MinecraftClientAccessor", "SplashScreenMixin", "ShaderLoaderAccessor", "ShaderLoaderCacheAccessor" -> Cfg.progressGui;
            default -> throw new IllegalStateException("Unrecognized mixin! This should never happen (was " + mixinClassName + ")");
        };
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {
    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {
    }
}
