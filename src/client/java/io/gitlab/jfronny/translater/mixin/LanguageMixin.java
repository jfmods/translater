package io.gitlab.jfronny.translater.mixin;

import io.gitlab.jfronny.translater.Translater;
import net.minecraft.client.resource.language.TranslationStorage;
import net.minecraft.util.Language;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.*;

import java.util.Map;

@Mixin(value = Language.class, priority = 900)
public class LanguageMixin {
    @ModifyVariable(at = @At("HEAD"), method = "setInstance", argsOnly = true)
    private static Language languageSetInstance(Language language) {
        if (language instanceof TranslationStorage t) {
            TranslationStorageAccessor ta = (TranslationStorageAccessor) t;
            ta.setTranslations(Translater.getMap(ta.getTranslations()));
            Translater.LOGGER.info("Set translater backend 2");
        } else Translater.LOGGER.error("Unsupported language configured: {}", language);
        return language;
    }

    @ModifyArg(
            at = @At(value = "INVOKE", target = "Lnet/minecraft/util/Language$1;<init>(Ljava/util/Map;)V"),
            method = "create()Lnet/minecraft/util/Language;",
            index = 0
    )
    private static Map<String, String> postCreate(Map<String, String> par1) {
        Translater.LOGGER.info("Set translater backend 1");
        return Translater.getMap(par1);
    }
}
