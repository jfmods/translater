package io.gitlab.jfronny.translater;

import io.gitlab.jfronny.libjf.config.api.v2.dsl.ConfigBuilder;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.Migration;

public class CfgMigration {
    @SuppressWarnings("UnstableApiUsage")
    public static ConfigBuilder<?> tweak(ConfigBuilder<?> cb) {
        return cb.addMigration("renderProgress", Migration.of(reader -> {
            String renderMode = reader.nextString();
            if (renderMode == null) return;
            switch (renderMode.toLowerCase()) {
                case "full" -> Cfg.progressGui = Cfg.progressConsole = Cfg.detailedProgress = true;
                case "gui" -> {
                    Cfg.progressConsole = Cfg.detailedProgress = false;
                    Cfg.progressGui = true;
                }
                case "console" -> {
                    Cfg.progressGui = Cfg.detailedProgress = false;
                    Cfg.progressConsole = true;
                }
                case "none" -> Cfg.progressGui = Cfg.progressConsole = Cfg.detailedProgress = false;
            }
        }));
    }
}
