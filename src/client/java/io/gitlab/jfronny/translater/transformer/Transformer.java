package io.gitlab.jfronny.translater.transformer;

import java.util.stream.Stream;

public interface Transformer {
    String transform(String str);
    void transformMultiple(Stream<? extends String> strings, ResultConsumer results);

    @FunctionalInterface
    interface ResultConsumer {
        void accept(String str, String translation);
    }
}
