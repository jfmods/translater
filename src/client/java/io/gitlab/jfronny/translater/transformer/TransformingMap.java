package io.gitlab.jfronny.translater.transformer;

import io.gitlab.jfronny.translater.Cfg;
import io.gitlab.jfronny.translater.RenderScheduler;
import io.gitlab.jfronny.translater.Translater;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class TransformingMap implements Map<String, String> {
    private final Transformer transformer;
    private final RenderScheduler renderScheduler = new RenderScheduler();
    private Map<String, String> backer = null;
    public boolean initializing = false;
    private AtomicInteger initProgress = null;
    private int maxProgress = 0;

    public TransformingMap(Transformer t) {
        transformer = t;
    }

    public void updateBacker(Map<String, String> backer) {
        if (this.backer != backer) {
            this.backer = backer;
            Collection<String> strings = backer.values();
            initializing = true;
            generateTranslations(strings);
            initializing = false;
        }
    }

    private void generateTranslations(Collection<? extends String> strings) {
        maxProgress = strings.size();
        initProgress = new AtomicInteger();
        transformer.transformMultiple(strings.parallelStream(), (str, translation) -> {
            int i = initProgress.incrementAndGet();
            if (i % 10 == 0 || Cfg.detailedProgress) {
                if (Cfg.progressConsole) Translater.LOGGER.info(getInitProgress());
                if (Cfg.progressGui && i % 10 == 0) renderScheduler.scheduleRender();
            }
        });
        renderScheduler.deschedule();
        initProgress = null;
    }

    public String getInitProgress() {
        if (initProgress == null || !initializing) throw new IllegalStateException("Tried to get init progress while not initializing");
        return "Transforming %d/%d".formatted(initProgress.get(), maxProgress);
    }

    @Override
    public int size() {
        return backer.size();
    }

    @Override
    public boolean isEmpty() {
        return backer.isEmpty();
    }

    @Override
    public boolean containsKey(Object o) {
        return backer.containsKey(o);
    }

    @Override
    public boolean containsValue(Object o) {
        return backer.containsValue(o);
    }

    @Override
    public String get(Object o) {
        if (o instanceof String s && s.startsWith("translater.")) return backer.get(s);
        return transformer.transform(backer.get(o));
    }

    @Override
    public String put(String s, String s2) {
        transformer.transform(s2);
        return backer.put(s, s2);
    }

    @Override
    public String remove(Object o) {
        return backer.remove(o);
    }

    @Override
    public void putAll(Map<? extends String, ? extends String> map) {
        generateTranslations(map.values());
        backer.putAll(map);
    }

    @Override
    public void clear() {
        backer.clear();
    }

    @Override
    public Set<String> keySet() {
        return backer.keySet();
    }

    @Override
    public Collection<String> values() {
        return backer.values(); // This does not support updates, but I don't care
    }

    @Override
    public Set<Entry<String, String>> entrySet() {
        return backer.entrySet(); // This does not support updates, but I don't care
    }
}
