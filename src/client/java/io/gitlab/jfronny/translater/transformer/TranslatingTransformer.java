package io.gitlab.jfronny.translater.transformer;

import io.gitlab.jfronny.libjf.translate.api.Language;
import io.gitlab.jfronny.libjf.translate.api.TranslateException;
import io.gitlab.jfronny.libjf.translate.api.TranslateService;
import io.gitlab.jfronny.translater.Cfg;
import io.gitlab.jfronny.translater.Translater;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.util.function.Function.identity;

public class TranslatingTransformer<T extends Language> implements Transformer {
    public TranslatingTransformer(TranslateService<T> ts) {
        rnd = new Random();
        this.ts = ts;
        languages = ts.getAvailableLanguages();
        languageAuto = ts.parseLang("auto");
        languageEnglish = ts.parseLang("en");
    }

    private static final Pattern SURROUNDING_SPACE_PATTERN = Pattern.compile("^(\\s*)(.*\\S+)(\\s*)$", Pattern.MULTILINE);
    private final Random rnd;
    private final TranslateService<T> ts;
    private final List<T> languages;
    private final T languageAuto;
    private final T languageEnglish;

    @Override
    public String transform(String str) {
        try {
            return tryTransform(str);
        } catch (Exception e) {
            Translater.LOGGER.warn("Failed to transform: \"{}\" ({} characters)! Please report this bug with the mod containing the lang file! ({})", str, str.length(), e.getLocalizedMessage());
            return null;
        }
    }

    private String tryTransform(String str) throws TranslateException {
        if (str.contains("%")) {
            StringBuilder res = new StringBuilder();
            boolean f = true;
            for (String s : str.split("%")) {
                if (!f) {
                    res.append("%");
                    if (!s.isEmpty()) {
                        appendTransformed(res.append(s.charAt(0)), s.substring(1), x -> x.replace("%", ""));
                    }
                } else appendTransformed(res, s, identity());
                f = false;
            }
            return res.toString();
        } else if (str.contains("$")) {
            StringBuilder res = new StringBuilder();
            boolean f = true;
            for (String s : str.split("\\$")) {
                if (f) appendTransformed(res, s, identity());
                else appendTransformed(res.append("$").append(s.charAt(0)), s.substring(1), x -> x.replace("$", ""));
                f = false;
            }
            return res.toString();
        } else if (str.contains("§")) {
            StringBuilder res = new StringBuilder();
            boolean f = true;
            for (String s : str.split("§")) {
                if (f) appendTransformed(res, s, identity());
                else appendTransformed(res.append("§").append(s.charAt(0)), s.substring(1), x -> x.replace("§", ""));
                f = false;
            }
            return res.toString();
        } else {
            return translateMultiple(str);
        }
    }

    private void appendTransformed(StringBuilder sb, String transformable, Function<String, String> fix) throws TranslateException {
        if (transformable.isEmpty()) return;
        sb.append(fix.apply(tryTransform(transformable)));
    }

    @Override
    public void transformMultiple(Stream<? extends String> strings, ResultConsumer results) {
        strings.flatMap(s -> {
            try {
                return Stream.of(new Translation(s, tryTransform(s)));
            } catch (Exception e) {
                return Stream.empty();
            }
        }).forEach(t -> results.accept(t.from, t.to));
    }

    private record Translation(String from, String to) {}

    private String translateMultiple(String str) throws TranslateException {
        Matcher m = SURROUNDING_SPACE_PATTERN.matcher(str);
        if (!m.find()) {
            if (Cfg.detailedProgress) Translater.LOGGER.info("Skipping translation of \"{}\"", str);
            return str;
        }
        try {
            T startLang = StringUtils.isAlpha(Cfg.targetLanguage) && Cfg.targetLanguage.length() == 2
                    ? ts.parseLang(Cfg.targetLanguage)
                    : languageAuto; // Identify target language
            String currentState = m.group(2);
            T currentLang = startLang;
            for (int i = 0; i < Cfg.rounds; i++) { // Translate around
                T newLang = randomLanguage();
                currentState = ts.translate(currentState, Cfg.breakFully ? randomLanguage() : currentLang, newLang);
                currentLang = newLang;
            }
            currentState = ts.translate(currentState, currentLang, startLang == languageAuto ? languageEnglish : startLang); // Translate to starting language
            currentState = m.group(1) + currentState + m.group(3); // Add back surrounding white space
            if (Cfg.detailedProgress) Translater.LOGGER.info("Transformed: \"{}\" to: \"{}\"", str, currentState);
            return currentState;
        } catch (Exception e) {
            String s = m.group(2);
            Translater.LOGGER.warn("Failed to break: \"{}\" ({} characters). Is your API key valid? ({})", s, s.length(), e.getLocalizedMessage());
            throw e;
        }
    }

    private T randomLanguage() {
        return languages.get(rnd.nextInt(languages.size()));
    }
}
