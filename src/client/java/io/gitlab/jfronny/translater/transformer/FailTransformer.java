package io.gitlab.jfronny.translater.transformer;

import java.util.stream.Stream;

public class FailTransformer implements Transformer {
    @Override
    public String transform(String str) {
        return null;
    }

    @Override
    public void transformMultiple(Stream<? extends String> strings, ResultConsumer results) {
    }
}
