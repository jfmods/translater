package io.gitlab.jfronny.translater;

import io.gitlab.jfronny.libjf.config.api.v2.Entry;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;

@JfConfig(referencedConfigs = "libjf-translate-v1", tweaker = CfgMigration.class)
public class Cfg {
    @Entry public static int rounds = 5;
    @Entry public static boolean breakFully = false;
    @Entry public static String targetLanguage = "en";
    @Entry public static boolean progressGui = false;
    @Entry public static boolean progressConsole = true;
    @Entry public static boolean detailedProgress = false;
    @Entry public static boolean forceRegenerate = false;
    @Entry public static boolean useDefaultCache = true;

    static {
        JFC_Cfg.ensureInitialized();
    }
}
